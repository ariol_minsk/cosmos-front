const fileinclude = require('gulp-file-include');

let project_folder = "public/frontend"; // финальная папка для заказчика
let source_folder = "resources"; // папка с исходным кодом в которой мы работаем


let path = {
    //пути выгрузок файлов
    build:{
        html: project_folder + "/../",
        css: project_folder + "/css/",
        js: project_folder + "/js/",
        img: project_folder + "/img/",
        fonts: project_folder + "/fonts/",
        svg: project_folder + "/svg-sprite/",
    },
    src:{
        html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],  //исключаем файлы с подчеркиванием
        css: source_folder + "/scss/style.scss", //gulp будет обрабатывать не все файлы а только 1 который будет собирать в себе все подключенные файлы
        js: source_folder + "/js/script.js", //фналогично с css-scss выше
        img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}", //слушать все папки и подпапки картинок и только такие расширения
        fonts: source_folder + "/fonts/*.ttf", //только ttf
        svg: source_folder + "/svg/**/*.svg",
    },
    //пути к файлам которые нужно слушать постоянно (изменения в них) и сразу что-то выполнять
    watch:{
        html: source_folder + "/**/*.html", //слушаем всё что html
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
        svg: source_folder + "/svg/**/*.svg",
    },
    clean: "./" + project_folder + "/"   //путь для удаления папки каждый раз при запуске галпа

}


//переменные для плагинов и дополнений 
let {src, dest } = require('gulp'),
    gulp = require('gulp'),
    browsersync = require('browser-sync').create(),
    myfileinclude = require("gulp-file-include"),
    del = require("del"),
    scss = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    clean_css = require("gulp-clean-css"),
    rename = require("gulp-rename"),
    svgSprite = require("gulp-svg-sprite"),
    imagemin = require("gulp-imagemin"),
    change = require('gulp-change'),
    uglify = require("gulp-uglify-es").default;


//запукаем сервер и плагин.
function browserSync(params){
    browsersync.init({
        server:{
            baseDir: "./" + project_folder + "/../"
        },
        port: 3600,
        notify: false //выключает кастомную табличку "Браузер обновился", чтобы не бесила!
    })
}

function html(){
    return src(path.src.html)  //обращение к исходникам
        .pipe(myfileinclude()) //сборка include htmlных файлов
        .pipe(change(function(content){
            return content.replace(/="\//g, '="./')
        }))
        .pipe(dest(path.build.html))  //выгрузка их в результат//pipe (функция внутри которой пишем команды для галпа). Тут осуществялеем переброс файлов  в папку dist
        .pipe(browsersync.stream()) //обновление страницы
}


function css(){
    return src(path.src.css)
        .pipe(
            scss({
                outputStyle: "expanded", //формирование не сжатого развёрнутого css файла!
            })
        )
        .pipe(
            autoprefixer({ //вендерные автопрефиксы 
                overrideBrowserslist: ["last 5 versions"],
                cascade: true
            })
        )
        .pipe(dest(path.build.css)) //выгружаем ещё не сжатый файл (если он не нужен, то можно закомментировать эту строку)
        .pipe(clean_css())  //делает сжатый файл, но его неудобно читать
        .pipe(
            rename({
               extname: ".min.css"   //переименовываем наш сжатый файл
            })
        )
        .pipe(dest(path.build.css)) 
        .pipe(browsersync.stream()) 
}


function js(){
    return src(path.src.js)
        .pipe(myfileinclude()) //собираем js файлы по частям в какой-то 1 файл
        .pipe(dest(path.build.js))
        .pipe(
            uglify()  //сжатие js
        )
        .pipe(
            rename({
                extname:".min.js"
            })
        )
        .pipe(dest(path.build.js)) 
        .pipe(browsersync.stream())
}

//картинки
function images(){
    return src(path.src.img)
        // .pipe(
        //     imagemin({
        //         progressive:true,
        //         svgoPlugins: [{removeViewBox: false}],
        //         interlaced: true,
        //         optimizationLevel: 3 // от 0 до 7
        //     })
        // ) 
        .pipe(dest(path.build.img))  
        .pipe(browsersync.stream())
}

function svg() {
    return src(path.src.svg)
      //  .pipe(imagemin())
        .pipe(
            svgSprite({
                mode: {
                    symbol: {
                        sprite: "../icons.svg",
                    },
                },
            })
        )
        .pipe(dest(path.build.svg));
}

//слежка за файлами при их изменении
function watchFiles(params){
    gulp.watch([path.watch.html], html); // слежка за html
    gulp.watch([path.watch.css], css); //это меняет на ходу изменения в css т.е мы пишем в scss и получаем это сразу в css
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.img], images);
    gulp.watch([path.watch.svg], svg);
}

//функция которая удаляет папку dist
function clean(params){
    return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, css, html, images, svg)); //функции которые должны выполняться
let watch = gulp.parallel(build, watchFiles, browserSync); 

exports.images = images;
exports.js = js;
exports.css = css;
exports.build = build;
exports.html = html;
exports.watch = watch; //дружим галп с переменной (вшиваем её в галп);
exports.default = watch; //при запуске галапа ЭТА переменная выполняется по умолчанию.







