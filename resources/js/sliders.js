const swiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        //   nextEl: '.swiper-button-next',
        //   prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper2 = new Swiper('#main-slider2.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        //   nextEl: '.swiper-button-next',
        //   prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper3 = new Swiper('.tester', {

    direction: 'horizontal',
    // loop: true,
    slidesPerView: 4,
    spaceBetween: 60,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper4 = new Swiper('.swiper.students', {
    observeParents: true,
    observer: true,
    observeSlideChildren: true,

    grabCursor: true,

    direction: 'horizontal',
    slidesPerView: 4,
    spaceBetween: 20,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});


const swiper99 = new Swiper('.swipertest', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 40,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1.5,
            spaceBetween: 20,
            width: 345
        },
        // when window width is >= 480px
        575: {
            slidesPerView: 1.5,
            spaceBetween: 30,
            width: 345
        },
        // when window width is >= 640px
        800: {
            slidesPerView: 3.5,
            spaceBetween: 40
        },
        1200: {
            slidesPerView: 3.5,
            spaceBetween: 40
        },
        1322: {
            slidesPerView: 3,
            spaceBetween: 40
        },
        1620: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        1920: {
            slidesPerView: 3,
            spaceBetween: 60
        }
    }
    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
});
const swiper98 = new Swiper('.swipertest2', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 'auto',
    // spaceBetween: 23,
    //width:345,
    // centeredSlides: true,
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 'auto',
            centeredSlides: true,
            touchRatio:0
        },
        // // when window width is >= 480px
        575: {
            // centeredSlides: false
        },
        // // when window width is >= 640px
        // 800: {
        //     slidesPerView: 3,
        // },
        // 1200: {
        //     slidesPerView: 3.5,
        // },
        // 1322: {
        //     slidesPerView: 3,
        // },
        // 1620: {
        //     slidesPerView: 4,
        // },
        // 1920: {
        //     slidesPerView: 3,
        // }
    }
    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
});


// const swiper12 = new Swiper('.swiper.yr_2', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
let courseLevels = null;

function swiperResize() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_2")
    ) {
        new Swiper(".swiper.yr_2", {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,
            watchOverflow: true,
            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_2")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_2", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize();
window.addEventListener("resize", function () {
    swiperResize();
})
// const swiper13 = new Swiper('.swiper.yr_3', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_3() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_3")
    ) {
        new Swiper(".swiper.yr_3", {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,
            watchOverflow: true,
            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_3")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_3", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_3();
window.addEventListener("resize", function () {
    swiperResize_yr_3();
})

// const swiper14 = new Swiper('.swiper.yr_6', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_6() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_6")
    ) {
        new Swiper('.swiper.yr_6', {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,

            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_6")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_6", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_6();
window.addEventListener("resize", function () {
    swiperResize_yr_6();
})

// const swiper15 = new Swiper('.swiper.yr_7', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_7() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_7")
    ) {
        new Swiper('.swiper.yr_7', {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,

            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_7")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_7", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_7();
window.addEventListener("resize", function () {
    swiperResize_yr_7();
})