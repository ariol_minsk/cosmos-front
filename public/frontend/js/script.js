const swiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        //   nextEl: '.swiper-button-next',
        //   prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper2 = new Swiper('#main-slider2.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        //   nextEl: '.swiper-button-next',
        //   prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper3 = new Swiper('.tester', {

    direction: 'horizontal',
    // loop: true,
    slidesPerView: 4,
    spaceBetween: 60,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

const swiper4 = new Swiper('.swiper.students', {
    observeParents: true,
    observer: true,
    observeSlideChildren: true,

    grabCursor: true,

    direction: 'horizontal',
    slidesPerView: 4,
    spaceBetween: 20,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});


const swiper99 = new Swiper('.swipertest', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 40,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1.5,
            spaceBetween: 20,
            width: 345
        },
        // when window width is >= 480px
        575: {
            slidesPerView: 1.5,
            spaceBetween: 30,
            width: 345
        },
        // when window width is >= 640px
        800: {
            slidesPerView: 3.5,
            spaceBetween: 40
        },
        1200: {
            slidesPerView: 3.5,
            spaceBetween: 40
        },
        1322: {
            slidesPerView: 3,
            spaceBetween: 40
        },
        1620: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        1920: {
            slidesPerView: 3,
            spaceBetween: 60
        }
    }
    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
});
const swiper98 = new Swiper('.swipertest2', {
    // Optional parameters
    direction: 'horizontal',
    slidesPerView: 'auto',
    // spaceBetween: 23,
    //width:345,
    // centeredSlides: true,
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 'auto',
            centeredSlides: true,
            touchRatio:0
        },
        // // when window width is >= 480px
        575: {
            // centeredSlides: false
        },
        // // when window width is >= 640px
        // 800: {
        //     slidesPerView: 3,
        // },
        // 1200: {
        //     slidesPerView: 3.5,
        // },
        // 1322: {
        //     slidesPerView: 3,
        // },
        // 1620: {
        //     slidesPerView: 4,
        // },
        // 1920: {
        //     slidesPerView: 3,
        // }
    }
    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
});


// const swiper12 = new Swiper('.swiper.yr_2', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
let courseLevels = null;

function swiperResize() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_2")
    ) {
        new Swiper(".swiper.yr_2", {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,
            watchOverflow: true,
            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_2")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_2", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize();
window.addEventListener("resize", function () {
    swiperResize();
})
// const swiper13 = new Swiper('.swiper.yr_3', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_3() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_3")
    ) {
        new Swiper(".swiper.yr_3", {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,
            watchOverflow: true,
            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_3")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_3", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_3();
window.addEventListener("resize", function () {
    swiperResize_yr_3();
})

// const swiper14 = new Swiper('.swiper.yr_6', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_6() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_6")
    ) {
        new Swiper('.swiper.yr_6', {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,

            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_6")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_6", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_6();
window.addEventListener("resize", function () {
    swiperResize_yr_6();
})

// const swiper15 = new Swiper('.swiper.yr_7', {
//     observeParents: true,
//     observer: true,
//     observeSlideChildren: true,
//
//     grabCursor: true,
//
//     direction: 'horizontal',
//     slidesPerView: 4,
//     spaceBetween: 20,
//
//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },
//
//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },
//
// });
function swiperResize_yr_7() {
    if (
        window.innerWidth > 992 &&
        document.querySelector(".swiper.yr_7")
    ) {
        new Swiper('.swiper.yr_7', {
            observeParents: true,
            observer: true,
            observeSlideChildren: true,

            grabCursor: true,

            direction: 'horizontal',
            slidesPerView: 4,
            spaceBetween: 20,
            minWidth: 280,
            maxWidth: 345,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }
    if (
        window.innerWidth > 768 &&
        document.querySelector(".swiper.yr_7")
    ) {
        if (!courseLevels) {
            courseLevels = new Swiper(".swiper.yr_7", {
                slidesPerView: "auto",
            });
        }
    } else if (courseLevels) {
        courseLevels.destroy(true, true);
        courseLevels = null;
    }
}

swiperResize_yr_7();
window.addEventListener("resize", function () {
    swiperResize_yr_7();
})
;document.querySelectorAll('.tabs-tv-triggers__item').forEach((item) =>
    item.addEventListener('click', function(e){
        e.preventDefault();
        const id = e.target.getAttribute('href').replace('#', '');
        console.log('был клик по , ' + id);

        document.querySelectorAll('.tabs-tv-triggers__item').forEach(
            (child) => child.classList.remove('tabs-tv-triggers__item--active')
        );

        document.querySelectorAll('.tabs-tv-content__item').forEach(
            (child) => child.classList.remove('tabs-tv-content__item--active')
        );

        item.classList.add('tabs-tv-triggers__item--active');
        document.getElementById(id).classList.add('tabs-tv-triggers__item--active');

        item.classList.add('tabs-tv-content__item--active');
        document.getElementById(id).classList.add('tabs-tv-content__item--active');
    })
);


let test2 = document.getElementsByClassName('tabs-tv-triggers__item');
if(test2.length==0){
    // alert('нет элемента')
}else{
    document.querySelector('.tabs-tv-triggers__item').click();
    // alert('есть элемент')
}

// if(document.getElementsByClassName('tabs-tv-triggers__item')){
//     document.querySelector('.tabs-tv-triggers__item').click();
//     // alert('есть элемент') 
// }else{
//     // alert('нет элемента')
// }
// document.querySelector('.tabs-tv-triggers__item').click();
;document.querySelectorAll('.tabs-triggers__item').forEach((item) =>
    item.addEventListener('click', function(e){
        e.preventDefault();
        const id = e.target.getAttribute('href').replace('#', '');
        console.log('был клик по , ' + id);
        //логика на слайдер
        if(id=="tab-2"){
            document.querySelectorAll('.test_tabs_abs').forEach(
                (child) => child.classList.remove('hidden')
            );
        }else{
            document.querySelectorAll('.test_tabs_abs').forEach(
                (child) => child.classList.add('hidden')
            );
        }
        //конец логики на слайдер


        //юридические табы
        if(id=="tab-1"){
            document.querySelectorAll('.yr_2_sl_c').forEach(
                (child) => child.classList.remove('hidden')
            );
        }else{
            document.querySelectorAll('.yr_2_sl_c').forEach(
                (child) => child.classList.add('hidden')
            );
        }

        if(id=="tab-2"){
            document.querySelectorAll('.yr_3_sl_c').forEach(
                (child) => child.classList.remove('hidden')
            );
        }else{
            document.querySelectorAll('.yr_3_sl_c').forEach(
                (child) => child.classList.add('hidden')
            );
        }
        //конец логики юр лиц


        document.querySelectorAll('.tabs-triggers__item').forEach(
            (child) => child.classList.remove('tabs-triggers__item--active')
        );

        document.querySelectorAll('.tabs-content__item').forEach(
            (child) => child.classList.remove('tabs-content__item--active')
        );

        item.classList.add('tabs-triggers__item--active');
        document.getElementById(id).classList.add('tabs-triggers__item--active');

        item.classList.add('tabs-content__item--active');
        document.getElementById(id).classList.add('tabs-content__item--active');
    })
);


let test = document.getElementsByClassName('tabs-triggers__item');
if(test.length==0){
    // alert('нет элемента')
}else{
    document.querySelector('.tabs-triggers__item').click();
    // alert('есть элемент')
}

// if(document.getElementsByClassName('tabs-triggers__item')){
//     document.querySelector('.tabs-triggers__item').click();
//     // alert('есть элемент') 
// }else{
//     // alert('нет элемента')
// }

// document.querySelector('.tabs-triggers__item').click();


//клик на таб и открытие блока
// st_slider.onclick = function() {
//     // let st_slider_let = document.getElementsByClassName('');

//     document.querySelectorAll('.test_tabs_abs').forEach(
//         (child) => child.classList.remove('hidden')
//     );
// };
;document.querySelectorAll('.tabs-place-triggers__item').forEach((item) =>
    item.addEventListener('click', function(e){
        e.preventDefault();
        const id = e.target.getAttribute('href').replace('#', '');
        console.log('был клик по , ' + id);

        document.querySelectorAll('.tabs-place-triggers__item').forEach(
            (child) => child.classList.remove('tabs-place-triggers__item--active')
        );

        document.querySelectorAll('.tabs-place-content__item').forEach(
            (child) => child.classList.remove('tabs-place-content__item--active')
        );

        item.classList.add('tabs-place-triggers__item--active');
        document.getElementById(id).classList.add('tabs-place-triggers__item--active');
        
        // alert(id);
        item.classList.add('tabs-place-content__item--active');
        document.getElementById(id).classList.add('tabs-place-content__item--active');
    })
);
let test3 = document.getElementsByClassName('tabs-place-triggers__item');
if(test3.length==0){
    // alert('нет элемента')
}else{
    document.querySelector('.tabs-place-triggers__item').click();
    // alert('есть элемент')
}
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}



var accs = document.getElementsByClassName("burger_menu")
var i;

for (i = 0; i < accs.length; i++) {
    accs[i].addEventListener("click", function() {
        console.log('кликнули')
        this.classList.toggle("active");
        var menu = document.getElementById("active_menu");
        if (menu.style.display === "block") {
            menu.style.display = "none";

            var btn = document.getElementById("active_burger");
            btn.style.display = "none";
            var btn2 = document.getElementById("not_active_burger");
            btn2.style.display = "flex";

        } else {
            menu.style.display = "block";
            var btn = document.getElementById("active_burger");
            btn.style.display = "flex";
            var btn2 = document.getElementById("not_active_burger");
            btn2.style.display = "none";
        }

        
    });
}






